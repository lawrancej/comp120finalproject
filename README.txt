In a small team (No more than 2-3 people), make a game.

It's okay to use code from elsewhere,
just cite it and commit just that code.

Ideally, the game should be playable.
And preferably complete.

Ideas:
Adventure / MUD
Minesweeper
Checkers
Maze
Tile sliding game
Concentration/Memory
Duck hunt
Paint
Conway's game of life
Hangman (two player)
Connect-four
Fence (dots/lines)
Scrabble
Galaga
frogger
Battleship (if it's done right)
pacman *
Card games (solitaire, texas holdem, freecell, spider, etc.)
Asteroids
Tetris *
Pong
Space invaders
Centipede / Snake
Tron
Pool
Breakout
